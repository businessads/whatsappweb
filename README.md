# Whatspup
Use Whatsapp from commandline/console/cli using [Bit Bucket Narendra!](https://bitbucket.org/ramnarendarmsit/whatsappweb/) :smile:

## Features ##

- Send and receive messages
- Read Receipts
- Switch between users to chat with
- Popup notifications for new chat messages
- Privacy settings for popup notifications
- One-time authentication, no need to scan QR code again and again
- Windowless/headless (hidden) mode
- Colorful chat messages

Of course, it is not possible to send/receive picture messages from command line.



## Requirements ##

- Node v8+
- puppeteer v0.13.0+

Tested on Windows with Node v8.9.1 and puppeteer v0.13.0

## Installation ##

- Clone this repository. `git clone https://ramnarendarmsit@bitbucket.org/ramnarendarmsit/whatsappweb.git`
- Type `npm install`
- Type `node chat.js` (case-sensitive)
- Chrome will open up, now just scan Whatsapp QR Code once via whatsapp app in your mobile
-- Info will be displayed after connection established
- Wait for connection and then execute our function calls as guided in the Info display


**NOTE:** Once you have connected by scanning QR code, your session will be saved so you won't have to scan it again and again unless you revoke from whatsapp app or by deleting **tmp** folder. 

## Commands ##

**Changing User**

You can switch chat with another user anytime by typing on console:
`--chat USERNAME` (case-sensitive)
`--forward USERNAME` (case-sensitive)

**NOTE:** `USERNAME` is supposed to be a person with whom you have already initiated a conversation in whatsapp. In other words, we use a selector to click that user's name from conversations list.

**Clear Chat Screen**

To clear chat screen, type `--clear`.

## Options ##

You can set various options in `config.js` file.

## Disclaimer ##

This project is not affiliated with official whatsapp in any sense.
