module.exports = {

    // user to chat with selector. We will "click" this selector so that chat window of
    // specified user is opened. XXX will be replaced by actual user.
    user_chat: '#pane-side span[title="XXX"]',

    // textbox selector where message will be typed
    chat_input: '#main > footer div.selectable-text[contenteditable]',

    // used to read last message sent by other person
    last_message: '#main div.message-in',

    
    // last message sent by you
    last_message_sent: '#main div.message-out span.selectable-text:last-child',
    last_message_sent1: '#main div.message-out span.selectable-text:last-child',
    last_gif_sent: '#main  div.message-out div._3CnDa',

    // last gif message sent by all _3CnDa
    last_gif_message_in: '#main div.message-in div._3CnDa',
    //_3_7SH _2hOiI message-in tail _1umJf _2quSw
    last_gif_message_out: '#main div.message-out div._3CnDa',
    //_3_7SH _2hOiI message-out tail
    //above one with double attempt
    //
    last_gif_message2: '#main div._3CnDa',
//[title="Forward"]
    forward_chat: '#main div.rAUz7:first-child',
//search chat text ... jN-F5 copyable-text selectable-text
search_input : 'input.jN-F5:last-child',
//chat_input: '#main > footer div.selectable-text[contenteditable]',
searc_user_select1 : 'div._2EXPL div.aZ91u:last-child',
searc_user_select : 'div._2EXPL:last-child',
search_user :'div._2EXPL',
//_3hV1n yavlE
forward_chat_send_button : 'div._3hV1n:last-child',
    // used to check if your messsage was read
    last_message_read: '#main div.message-out span[data-icon]:last-child',

    // gets username for conversation thread
    user_name: '#main > header span[title]',

    // checks if there are new messages by any users
    new_message_user: '#pane-side div.CxUIE span[title]'

}
