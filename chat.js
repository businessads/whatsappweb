#!/usr/bin / env node

const puppeteer = require('puppeteer');
const notifier = require('node-notifier');
const chalk = require('chalk');
const winston = require('winston');
const fs = require('fs');
const boxen = require('boxen');
const gradient = require('gradient-string');
const logSymbols = require('log-symbols');
const ansiEscapes = require('ansi-escapes');
const path = require('path');
const findChrome = require('./find_chrome');

const config = require('./config.js');
const selector = require('./selector.js');

const createCsvWriter = require('csv-writer').createObjectCsvWriter;

const csvWriter = createCsvWriter({
  path: 'file.csv',
  header: [
      {id: 'name', title: 'NAME'},
      {id: 'status', title: 'status'}
  ]
});

let contacts=[];

let records = [];
let user = '';

let iteration = 0;
fs.readFile('BroadCastListContacts.json', (err, data) => {  
  if (err) throw err;
  contacts= JSON.parse(data).BroadcastList[0].contacts;  
});

(async function main() {

  const logger = setUpLogging();

  try {

    let last_received_message = '';
    let last_received_message_other_user = '';
    let last_sent_message_interval = null;
    let sentMessages = [];
    ////////////////Naren custom variables//////////////////////////////    
    let modeOfSending = "text";
    ////////////////
    const executablePath = findChrome().pop() || null;
    const tmpPath = path.resolve(__dirname, config.data_dir);
    const networkIdleTimeout = 30000;
    const stdin = process.stdin;
    const headless = !config.window;

    const browser = await puppeteer.launch({
      headless: headless,
      executablePath: executablePath,
      userDataDir: tmpPath,
      args: [
        '--disable-gpu',
        '--disable-infobars',
        '--window-position=0,0',
        '--ignore-certificate-errors',
        '--ignore-certificate-errors-spki-list'
      ]
    });

    const page = await browser.newPage();

    print(gradient.rainbow('Initializing...\n'));

    page.goto('https://web.whatsapp.com/', {
      waitUntil: 'networkidle2',
      timeout: 0
    }).then(async function (response) {

      
      await page.waitFor(networkIdleTimeout);      
      
      //startChat(user);
    
   
      //debug(page);
      //startForwardChat();
      print(boxen('Info', {
        padding: 1,
        borderStyle: 'double',
        borderColor: 'green',
        backgroundColor: 'green'
      }));
      print(boxen('--chat <user>\n--forward <user>\n--getLastSentMessageLog\n--clear', {
        padding: 1,
        borderStyle: 'single',
        borderColor: 'green',
        backgroundColor: ''
      }));

      readCommands();
    })

    function readArguments(){
      process.setMaxListeners(0);

      // make sure they specified user to chat with
      if (!process.argv[2]) {
        console.log(logSymbols.error, chalk.red('User argument not specified, exiting...'));
        process.exit(1);
      }

      /////////////////////////////////////////////
      // get user from command line argument
      
      let forwardMessage = null;
      let selection = false;
            

      // because a username can contain first and last name/spaces, etc
      for (let i = 2; i <= 5; i++) {
        if (typeof process.argv[i] !== 'undefined') {
          user += process.argv[i] + ' ';
        }
      }

      user = user.trim();
      /////////////////////////////////////////////

      // catch un-handled promise errors
      process.on("unhandledRejection", (reason, p) => {
        //console.warn("Unhandled Rejection at: Promise", p, "reason:", reason);
      });
    }

    async function getLastSentMessageLog(){
      console.log(" contacts.length:"+ contacts.length)
      for (let i=0;i < contacts.length && i<5 ;i++){
        try{
          user = contacts[i];
          console.log("user initial lauch:"+user)
          let res = searchAndSelect(user);//contacts[i]
          await page.waitFor(3500);
          console.log("res:"+res+",selection:"+selection)
          
          let messageSent  ="-1_-1_-1_-1";
          if(selection){
              await page.waitFor(3500);
              messageSent = await page.evaluate((selector) => {

                let nodes = document.querySelectorAll(selector);
                let el = nodes[nodes.length - 1];

                let checkVal=-1,dblcheckVal=-1,dblcheckAckVal=-1;
                let readHTML = '';
                if (el) {
                  //for text message
                  //readHTML = el.parentElement.parentElement.parentElement.outerHTML;
                  readHTML = el.parentElement.outerHTML;
                // checkVal=0,dblcheckVal=0,dblcheckAckVal=0;
                  //if(el.parentElement.parentElement.parentElement.getElementsByClassName('msg-check'))
                  
                    checkVal = readHTML.indexOf('data-icon="msg-check"');
                    if(checkVal<0)
                      checkVal = readHTML.indexOf('data-icon="msg-check-light"');
                  //if(el.parentElement.parentElement.parentElement.getElementsByClassName('msg-dblcheck'))
                    dblcheckVal = readHTML.indexOf('data-icon="msg-dblcheck"');
                    if(dblcheckVal<0)
                      dblcheckVal = readHTML.indexOf('data-icon="msg-dblcheck-light"');
                  //if(el.parentElement.parentElement.parentElement.getElementsByClassName('msg-dblcheck-ack'))
                    dblcheckAckVal =  readHTML.indexOf('data-icon="msg-dblcheck-ack"');
                    if(dblcheckAckVal<0)
                      dblcheckAckVal = readHTML.indexOf('data-icon="msg-dblcheck-ack-light"');
                    //msg-dblcheck-ack-light
                    
                }
                //return el ? readHTML+"_"+
                return checkVal+"_"+dblcheckVal+"_"+dblcheckAckVal;
                
              }, selector.last_gif_sent);
              
             // await page.waitFor(1500);
              console.log("messageSent:"+messageSent);              
              await page.waitFor(1500);
          }
          else
            await page.waitFor(1000);
     info = messageSent.split("_");
     status = "Not sent";
     if(info[0]*1>-1){
      status = "Not Delivered"
     }
     else if(info[1]*1>-1){
      status = "Delivered and Not seen"
     }
     else if(info[2]*1>-1){
      status = "Delivered and seen"
     }


        print(user+" Last message was " + status, config.sent_message_color);
        temp ={}
        temp['name']=user;
        temp['status']=status;
        records.push(temp)
        // setup interval for read receipts
       // if (config.read_receipts) {
          // last_sent_message_interval = setInterval(function () {
            // isLastMessageRead(user, message);
     // console.log("user :"+user+" our last sent message read status is :"+ isLastMessageRead(user, messageSent));
           
          // }, (config.check_message_interval));
       // }
      
     // await page.waitFor(1500);
      
    }catch(er){
      console.log("user notfound: er:"+er)
    }
 }
 csvWriter.writeRecords(records)       // returns a promise
    .then(() => {
        console.log('...Done');
        
        
    });
    user = "Aditya";
    await page.waitFor(1500);
        searchAndSelect(user);
        await page.waitFor(1500);
        await page.click(selector.chat_input);
        await page.waitFor(4000);

        await page.keyboard.type(JSON.stringify(records));
        //await page.keyboard.press('Enter');

    }

    // allow user to type on console and read it
    function readCommands() {
      
  
  
      stdin.resume();

      stdin.on('data',async function (data) {
        let message = data.toString().trim();
        // check for command "--chat UserName" to start new chat with that user
        if (message.toLowerCase().indexOf('--chat') > -1) {
          console.log("--chat calling")
          let new_user = message.split(" ").slice(1).join(" ");
          modeOfSending = "chat";
          if (new_user) {
            startChat(new_user);
            user = new_user;
          } else {
            console.log(logSymbols.error, chalk.red('user name not specified!'));
          }
        }
        // clear chat screen
        else if (message.toLowerCase().indexOf('--clear') > -1) {          
          console.log("--clear calling:")
          process.stdout.write(ansiEscapes.clearScreen);
        }else if (message.toLowerCase().indexOf('--forward') > -1) {
          console.log("forward chat");
          let new_user = message.split(" ").slice(1).join(" ");          
          if (new_user) {
            if(user!=new_user){
              searchAndSelect(new_user);
              await page.waitFor(4000);
            }
            modeOfSending = "forward";
            user = new_user;
            startForwardChat()
            
          } else {
            console.log(logSymbols.error, chalk.red('user name not specified!'));
          }
        }else if (message.toLowerCase().indexOf('--getlastsentmessagelog') > -1) {
          console.log("calling getLastSentMessageLog")
          getLastSentMessageLog();
        } else {
          if(modeOfSending != "forward")
            typeMessage(message);
          else
            forwardChat(message);
        }
        stdin.resume();
      });
    }

    // start chat with specified user
    async function startChat(user) {
      console.log("challed for modified code user:"+user);
      // replace selector with selected user
      let user_chat_selector = selector.user_chat;
      user_chat_selector = user_chat_selector.replace('XXX', user);

      await page.waitFor(user_chat_selector);

      await page.click(user_chat_selector);
      await page.click(selector.chat_input);

      let name = getCurrentUserName();
      if (name) {
        console.log(logSymbols.success, chalk.bgGreen('You can chat now :-)'));
        console.log(logSymbols.info, chalk.bgRed('Press Ctrl+C twice to exit any time.\n'));        
      } else {
        console.log(logSymbols.warning, 'Could not find specified user "' + user + '"in chat threads\n');
      }
      
    }
    async function searchAndSelect(user) {
      
      await page.click(selector.search_input);
      
      let input = await page.$('input');
      let status = -1;
          try{ 
            if(modeOfSending!="forward"){
              await input.click({ clickCount: 3 })
              await page.waitFor(300);
            
              await page.keyboard.type(user);
              await page.waitFor(1500);
                //let temp = el.querySelectorAll(selector.search_user);
                //console.log("nodes.length:"+temp.length);
                
                //if(temp.length>1)
                status = await page.evaluate(() => {
                  return document.getElementsByClassName('_2EXPL').length;
              });
              await page.waitFor(200);
              selection = false;
              if(status>=1){
                  await page.click(selector.searc_user_select);
                  
                selection = true;
                
              }
              console.log("status.len***:"+status)

            }
            else{
              status = 1;
              await page.click(selector.searc_user_select);
            }

            
          return status;

        }catch (err) {
          
         // await page.waitFor(1000);
          logger.warn("err0:"+err);  
          return status;     
        }
        return status;
    }

    // start chat with specified user
    async function startForwardChat() {
      await page.waitFor(4000);
      console.log("challed for switch user:"+user);
      // replace selector with selected user
      let messageSent = await page.evaluate((selector) => {

        let nodes = document.querySelectorAll(selector);
        let el = nodes[nodes.length - 1];
        
        if (!el) {
          return '';
        }

        // check if it is picture message

        /*
        if (el.classList.contains('message-image')) {
          return 'Picture Message';
        }
        */

        let picNodes = el.querySelectorAll("img[src*='blob']");
        let isPicture = picNodes[picNodes.length - 1];

        if (isPicture) {
          console.log("isPicture"+isPicture);
          return 'Picture Message';
        }

        // check if it is gif message
        let gifNodes = el.querySelectorAll("div[style*='background-image']");
        let isGif = gifNodes[gifNodes.length - 1];

        if (isGif) {
          let temp = document.querySelectorAll('#main  div.message-out div._3CnDa');
          temp[temp.length-1].click();
          console.log("isGif");
          return 'Gif Message';
        }

        // check if it is video message
        let vidNodes = el.querySelectorAll(".video-thumb");
        let isVideo = vidNodes[vidNodes.length - 1];

        if (isVideo) {
          console.log("isVideo"+isVideo);
          return 'Video Message';
        }

        // check if it is voice message
        let audioNodes = el.querySelectorAll("audio");
        let isAudio = audioNodes[audioNodes.length - 1];

        if (isAudio) {
          console.log("isAudio"+isAudio);
          return 'Voice Message';
        }

        // check if it is emoji message
        let emojiNodes = el.querySelectorAll("div.selectable-text img.selectable-text");
        let isEmoji = emojiNodes[emojiNodes.length - 1];

        if (isEmoji) {
          console.log("isEmoji:"+isEmoji);
          return 'Emoji Message';
        }

        // text message
        nodes = el.querySelectorAll('span.selectable-text');
        el = nodes[nodes.length - 1];

        console.log("el.innerText:"+el ? el.innerText : '');
        return el ? el.innerText : '';

      }, selector.last_gif_sent);
      // let args = forwardPreviousMessage();
      // console.log("forward chat called then waiting");
      // await page.waitFor(1000);
      
      // console.log("get reply"+args[0]);
      
      console.log("forward chat end***messageSent:"+messageSent);
      if(messageSent=="Gif Message"){
        
        
       try{
        //
        //let last_gif_message_out = await page.$('#main  div.message-out div._3CnDa:last-child');
        //console.log(" ********************************************************")
        //console.log(" last_gif_message_out.outerHTML:"+last_gif_message_out)
        //console.log(" last_gif_message_out"+ last_gif_message_out)
        //console.log(" ********************************************************")
        
//        await page.evaluate(()=>document.querySelectorAll('#main  div.message-out div._3CnDa')[5].click())
        //await last_gif_message_out.click({ clickCount: 1 });
        //await last_gif_message_out.click({ clickCount: 1 });
        //forward_chat
        await page.waitFor(700);
        await page.click(selector.forward_chat);
        await page.waitFor(500);
        await page.click(selector.search_input);
        let selection = false;
       let selectionCount = 0;
       let ignoredCount = 0;
         
        await page.waitFor(800);
       let input = await page.$('input');
       await page.waitFor(100);
       await input.click({ clickCount: 3 })
        for (let i=iteration;selectionCount<5 && i+ignoredCount+2 < contacts.length;i++){
          
         
          try{ 
            await page.waitFor(800);
           console.log("i:"+i+"*****contacts[i]{"+contacts[i]);
            await page.keyboard.type(contacts[i]);
            await page.waitFor(1000);
            //let temp = el.querySelectorAll(selector.search_user);
            //console.log("nodes.length:"+temp.length);
            
            //if(temp.length>1)
            let status = await page.evaluate(() => {
              return document.getElementsByClassName('JSbIY').length;
          });
          console.log("status.len:"+status)
          if(status>=1){
            selection = true;
            selectionCount++;
            try{
              await page.click(selector.searc_user_select);
            }catch (err) {
              logger.warn("err123:"+err);       
              await page.waitFor(100);
              await input.click({ clickCount: 3 })
            }
          }
          else{

            ignoredCount++;
          }
               
          await page.waitFor(100);
              await input.click({ clickCount: 3 })
              
          }catch (err) {
            logger.warn("err1err1:"+err);        
            await page.waitFor(100);
            await input.click({ clickCount: 3 })
          }
        }
        console.log("selection:"+selection)
        //_2wP_Y
        await page.waitFor(500);
       if(selection)
          await page.click(selector.forward_chat_send_button);
        
        iteration = iteration+selectionCount+ignoredCount;
        if(iteration < contacts.length){
        await page.waitFor(500);
        
        console.log("switch user calling:"+user)
       // startChat(user);
       searchAndSelect(user);
        await page.waitFor(3000);
        //debug(page);
          startForwardChat();
        }
      
      }catch (err) {
        logger.warn("err4:"+err);       
      }
      }
      
      
    }


    // forward chat with specified user
    async function forwardChat(message) {
      console.log("forward chat called***");
      // replace selector with selected user

      // see if they sent a new message
       // check if it is gif message
       
       let messageSent = await page.evaluate((selector) => {

        let nodes = document.querySelectorAll(selector);
        let el = nodes[nodes.length - 1];
        
        if (!el) {
          return '';
        }

        // check if it is picture message

        /*
        if (el.classList.contains('message-image')) {
          return 'Picture Message';
        }
        */

        let picNodes = el.querySelectorAll("img[src*='blob']");
        let isPicture = picNodes[picNodes.length - 1];

        if (isPicture) {
          console.log("isPicture"+isPicture);
          return 'Picture Message';
        }

        // check if it is gif message
        let gifNodes = el.querySelectorAll("div[style*='background-image']");
        let isGif = gifNodes[gifNodes.length - 1];

        if (isGif) {
          console.log("isGif");
          return 'Gif Message';
        }

        // check if it is video message
        let vidNodes = el.querySelectorAll(".video-thumb");
        let isVideo = vidNodes[vidNodes.length - 1];

        if (isVideo) {
          console.log("isVideo"+isVideo);
          return 'Video Message';
        }

        // check if it is voice message
        let audioNodes = el.querySelectorAll("audio");
        let isAudio = audioNodes[audioNodes.length - 1];

        if (isAudio) {
          console.log("isAudio"+isAudio);
          return 'Voice Message';
        }

        // check if it is emoji message
        let emojiNodes = el.querySelectorAll("div.selectable-text img.selectable-text");
        let isEmoji = emojiNodes[emojiNodes.length - 1];

        if (isEmoji) {
          console.log("isEmoji:"+isEmoji);
          return 'Emoji Message';
        }

        // text message
        nodes = el.querySelectorAll('span.selectable-text');
        el = nodes[nodes.length - 1];

        console.log("el.innerText:"+el ? el.innerText : '');
        return el ? el.innerText : '';

      }, selector.last_message_sent);
      // let args = forwardPreviousMessage();
      // console.log("forward chat called then waiting");
      // await page.waitFor(1000);
      
      // console.log("get reply"+args[0]);
      
      console.log("forward chat end***messageSent:"+messageSent);
      if(messageSent=="Gif Message"){
        
      await page.click(selector.last_gif_message);
      await page.click(selector.last_gif_message);
      //forward_chat
      await page.waitFor(1000);
      await page.click(selector.forward_chat);
      await page.waitFor(500);
      await page.click(selector.search_input);

      await page.waitFor(500);
      await page.keyboard.type("Wife");
      //_2wP_Y
      console.log("selector.search_input click select initiated2:"+selector.searc_user_select2);
      await page.waitFor(500);
      try{
      await page.click(selector.searc_user_select);
      }catch (err) {
        logger.warn("err0:");
       
      }
      console.log("selector.search_input adityachitra selected waiting");
      await page.waitFor(500);      
      await page.keyboard.type("Aditya Chitra");
      await page.waitFor(500);
      await page.click(selector.searc_user_select);
      console.log("selector.search_input waiting to click send button");
      await page.waitFor(500);
      
      await page.click(selector.forward_chat_send_button);
      
      }
    }


    // type user-supplied message into chat window for selected user
    async function typeMessage(message) {
      await page.keyboard.type(message);
      await page.keyboard.press('Enter');

      // verify message is sent
      let messageSent = await page.evaluate((selector) => {

        let nodes = document.querySelectorAll(selector);
        let el = nodes[nodes.length - 1];

        return el ? el.innerText : '';
      }, selector.last_message_sent);

      if (message == messageSent) {
        print("You: " + message, config.sent_message_color);

        // setup interval for read receipts
        if (config.read_receipts) {
          // last_sent_message_interval = setInterval(function () {
          //   isLastMessageRead(user, message);
          // }, (config.check_message_interval));
        }

      }

      // see if they sent a new message
      //readLastOtherPersonMessage();
    }

    // read user's name from conversation thread
    async function getCurrentUserName() {
      return await page.evaluate((selector) => {
        let el = document.querySelector(selector);

        return el ? el.innerText : '';
      }, selector.user_name);
    }

    // read any new messages sent by specified user
    async function readLastOtherPersonMessage() {

      let message = '';
      let name = await getCurrentUserName();

      if (!name) {
        return false;
      }
      // read last message sent by other user
      message = await page.evaluate((selector) => {
console.log("initialisation***************");
        let nodes = document.querySelectorAll(selector);
        let el = nodes[nodes.length - 1];

        if (!el) {
          return '';
        }

        // check if it is picture message

        /*
        if (el.classList.contains('message-image')) {
          return 'Picture Message';
        }
        */

        let picNodes = el.querySelectorAll("img[src*='blob']");
        let isPicture = picNodes[picNodes.length - 1];

        if (isPicture) {
          console.log("isPicture"+isPicture);
          return 'Picture Message';
        }

        // check if it is gif message
        let gifNodes = el.querySelectorAll("div[style*='background-image']");
        let isGif = gifNodes[gifNodes.length - 1];

        if (isGif) {
          console.log("isGif"+el.outerHTML);
          return 'Gif Message';
        }

        // check if it is video message
        let vidNodes = el.querySelectorAll(".video-thumb");
        let isVideo = vidNodes[vidNodes.length - 1];

        if (isVideo) {
          console.log("isVideo"+isVideo);
          return 'Video Message';
        }

        // check if it is voice message
        let audioNodes = el.querySelectorAll("audio");
        let isAudio = audioNodes[audioNodes.length - 1];

        if (isAudio) {
          console.log("isAudio"+isAudio);
          return 'Voice Message';
        }

        // check if it is emoji message
        let emojiNodes = el.querySelectorAll("div.selectable-text img.selectable-text");
        let isEmoji = emojiNodes[emojiNodes.length - 1];

        if (isEmoji) {
          console.log("isEmoji:"+isEmoji);
          return 'Emoji Message';
        }

        // text message
        nodes = el.querySelectorAll('span.selectable-text');
        el = nodes[nodes.length - 1];

        console.log("el.innerText:"+el ? el.innerText : '');
        return el ? el.innerText : '';

      }, selector.last_message);


      if (message) {
        if (last_received_message) {
          if (last_received_message != message) {
            last_received_message = message;
            print(name + ":*" + message, config.received_message_color);

            // show notification
            notify(name, message);
          }
        } else {
          last_received_message = message;
          //print(name + ": " + message, config.received_message_color);
        }

      }
    }

    // read any new messages sent by specified user
    async function forwardPreviousMessage() {

      let message = '';
      let name = await getCurrentUserName();

      if (!name) {
        return false;
      }
console.log("forward previous message");
      // read last message sent by other user
      message = await page.evaluate((selector) => {
console.log("selector ******");
        let nodes = document.querySelectorAll(selector);
        let el = nodes[nodes.length - 1];

        if (!el) {
          return '';
        }

        // check if it is picture message

        /*
        if (el.classList.contains('message-image')) {
          return 'Picture Message';
        }
        */

        let picNodes = el.querySelectorAll("img[src*='blob']");
        let isPicture = picNodes[picNodes.length - 1];

        if (isPicture) {
          console.log("isPicture:"+isPicture);
          return ['Picture Message',isPicture];
        }

        // check if it is gif message
        let gifNodes = el.querySelectorAll("div[style*='background-image']");
        let isGif = gifNodes[gifNodes.length - 1];

        if (isGif) {
          console.log("isGif:"+el.outerHTML);
          return 'Gif Message';
        }

        // check if it is video message
        let vidNodes = el.querySelectorAll(".video-thumb");
        let isVideo = vidNodes[vidNodes.length - 1];

        if (isVideo) {
          console.log("isVideo:"+isVideo);
          return ['Video Message',isVideo];
        }

        // check if it is voice message
        let audioNodes = el.querySelectorAll("audio");
        let isAudio = audioNodes[audioNodes.length - 1];

        if (isAudio) {
          console.log("isAudio:"+isAudio);
          return ['Voice Message',isAudio];
        }

        // check if it is emoji message
        let emojiNodes = el.querySelectorAll("div.selectable-text img.selectable-text");
        let isEmoji = emojiNodes[emojiNodes.length - 1];

        if (isEmoji) {
          console.log("isEmoji:"+isEmoji);
          return ['Emoji Message',isEmoji];
        }

        // text message
        nodes = el.querySelectorAll('span.selectable-text');
        el = nodes[nodes.length - 1];

        console.log("Text Message:"+el.innerText);
        return ['Text Message',el ? el.innerText : 'null'];

      }, selector.last_message);

      return message;
      
    }

    // checks if last message sent is read
    async function isLastMessageRead(name, message) {

      let is_last_message_read = await page.evaluate((selector) => {

        let nodes = document.querySelectorAll(selector);
        let el = nodes[nodes.length - 1];

        if (el) {
          let readHTML = el.outerHTML;
          let checkVal=0,dblcheckVal=0,dblcheckAckVal=0;
          if(el.getElementsByClassName('msg-check'))
            checkVal = el.getElementsByClassName('msg-check').length;
          if(el.getElementsByClassName('msg-dblcheck'))
            dblcheckVal = el.getElementsByClassName('msg-dblcheck').length;
          if(el.getElementsByClassName('msg-dblcheck-ack'))
            dblcheckAckVal = el.getElementsByClassName('msg-dblcheck-ack').length;
          
          if (readHTML.length) {//msg-check  //msg-dblcheck
            return checkVal+"_"+dblcheckVal+"_"+dblcheckAckVal+"_"+readHTML.indexOf('data-icon="msg-dblcheck-ack"') > -1;
          }
          return checkVal+"_"+dblcheckVal+"_"+dblcheckAckVal;
        }

        return false;
      }, selector.last_message_read);
      console.log("is_last_message_read:"+is_last_message_read);
      if (is_last_message_read) {
        if (config.read_receipts && last_sent_message_interval) {
          // make sure we don't report for same message again
          if (!sentMessages.includes(message)) {
            console.log('\n' + logSymbols.success, chalk.gray(message) + '\n');

            sentMessages.push(message);

            clearInterval(last_sent_message_interval);
          }
        }
      }

    }

    // checks for any new messages sent by all other users
    async function checkNewMessagesAllUsers() {
      let name = await getCurrentUserName();

      let user = await page.evaluate((selector) => {

        let nodes = document.querySelectorAll(selector);
        let el = nodes[0];

        return el ? el.innerText : '';
      }, selector.new_message_user);

      if (user && user != name) {
        let message = 'You have a new message by "' + user + '". Switch to that user to see the message.';

        if (last_received_message_other_user != message) {
          print('\n' + message, config.received_message_color_new_user);

          // show notification
          notify(user, message);

          last_received_message_other_user = message;
        }
      }
    }

    // prints on console
    function print(message, color = null) {

      if (!config.colors || color == null) {
        console.log('\n' + message + '\n');
        return;
      }

      if (chalk[color]) {
        console.log('\n' + chalk[color](message) + '\n');
      } else {
        console.log('\n' + message + '\n');
      }

    }

    // send notification
    function notify(name, message) {
      if (config.notification_enabled) {

        if (config.notification_hide_message) {
          message = config.notification_hidden_message || 'New Message Received';
        }

        if (config.notification_hide_user) {
          name = config.notification_hidden_user || 'Someone';
        }

        notifier.notify({
          appName: "Snore.DesktopToasts", // Windows FIX - might not be needed
          title: name,
          message: message,
          wait: false,
          timeout: config.notification_time
        });

        // sound/beep
        if (config.notification_sound) {
          process.stdout.write(ansiEscapes.beep);
        }

      }
    }

 //   setInterval(readLastOtherPersonMessage, (config.check_message_interval));
   // setInterval(checkNewMessagesAllUsers, (config.check_message_interval));

  } catch (err) {
    logger.warn(err);
  }

  async function debug(page, logContent = true) {
    if (logContent) {
      console.log(await page.content());
    }

    await page.screenshot({
      path: 'screen.png'
    });
  }

  // setup logging
  function setUpLogging() {

    const env = process.env.NODE_ENV || 'development';
    const logDir = 'logs';

    // Create the log directory if it does not exist
    if (!fs.existsSync(logDir)) {
      fs.mkdirSync(logDir);
    }

    const tsFormat = () => (new Date()).toLocaleTimeString();

    const logger = new(winston.Logger)({
      transports: [
        // colorize the output to the console
        new(winston.transports.Console)({
          timestamp: tsFormat,
          colorize: true,
          level: 'info'
        }),
        new(winston.transports.File)({
          filename: `${logDir}/log.log`,
          timestamp: tsFormat,
          level: env === 'development' ? 'debug' : 'info'
        })
      ]
    });

    return logger;
  }

})();